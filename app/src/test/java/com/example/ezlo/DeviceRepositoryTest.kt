package com.example.ezlo

import com.example.ezlo.data.local.DeviceDao
import com.example.ezlo.data.remote.api.DeviceRest
import com.example.ezlo.data.remote.model.DeviceDTO
import com.example.ezlo.data.remote.model.DeviceList
import com.example.ezlo.data.repository.DeviceRepositoryImpl
import com.example.ezlo.domain.repository.DeviceRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any

@ExperimentalCoroutinesApi
class DeviceRepositoryTest {

    @Mock
    private lateinit var deviceApi: DeviceRest

    @Mock
    private lateinit var deviceDao: DeviceDao

    private lateinit var deviceRepository: DeviceRepository

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        deviceRepository = DeviceRepositoryImpl(deviceApi, deviceDao)
    }

    @After
    fun tearDown() {
        Mockito.verifyNoMoreInteractions(deviceApi, deviceDao)
    }

    @Test
    fun `test resetDeviceList with successful response`() = runTest {
        val apiDevices = listOf(
            DeviceDTO(
                firmware = "1.7.455",
                internalIP = "192.168.6.213",
                lastAliveReported = "2018-02-20 04:17:43",
                macAddress = "e0:60:66:02:e2:1b",
                pkDevice = 45013855,
                pkDeviceSubType = 2,
                pkDeviceType = 1,
                platform = "Sercomm NA301",
                serverAccount = "vera-us-oem-account12.mios.com",
                serverDevice = "vera-us-oem-device12.mios.com",
                serverEvent = "vera-us-oem-event12.mios.com"
            ),
            DeviceDTO(
                firmware = "1.7.455",
                internalIP = "192.168.10.6",
                lastAliveReported = "2018-02-20 04:17:43",
                macAddress = "94:4a:0c:25:63:c4",
                pkDevice = 50100057,
                pkDeviceSubType = 2,
                pkDeviceType = 1,
                platform = "Sercomm G450",
                serverAccount = "vera-us-oem-account12.mios.com",
                serverDevice = "vera-us-oem-device12.mios.com",
                serverEvent = "vera-us-oem-event12.mios.com"
            )
        )

        val entities = apiDevices.map { it.toDeviceEntity() }
        Mockito.`when`(deviceApi.getListOfDevices()).thenReturn(DeviceList(devices = apiDevices))
        Mockito.`when`(deviceDao.deleteAllDevices()).thenReturn(Unit)
        Mockito.`when`(deviceDao.insertDevices(any())).thenReturn(Unit)

        deviceRepository.resetDeviceList()
        Mockito.verify(deviceApi).getListOfDevices()
        Mockito.verify(deviceDao).deleteAllDevices()
        Mockito.verify(deviceDao).insertDevices(entities)
    }


}
