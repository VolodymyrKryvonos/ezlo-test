package com.example.ezlo.domain.repository

import com.example.ezlo.domain.model.Device
import com.example.ezlo.ui.util.UiText
import com.example.ezlo.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow

interface DeviceRepository {
    val deviceList: StateFlow<List<Device>>
    val errorMessageFlow: SharedFlow<UiText>

    suspend fun loadDeviceList()
    suspend fun resetDeviceList()

    suspend fun updateDeviceTitle(deviceId: Long, newTitle: String)

    suspend fun deleteDevice(deviceId: Long)

}