package com.example.ezlo.domain.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.example.ezlo.R

data class Device(
    val index: Int,
    val title: String,
    val pkDevice: Long,
    val macAddress: String,
    val firmware: String,
    val platform: String
) {
    fun getPlatformImageDrawable(): Int {
        return platformToDrawableMap[platform]?.first
            ?: R.drawable.vera_edge_big
    }

    fun getModel(): Int {
        return platformToDrawableMap[platform]?.second
            ?: R.string.vera_edge
    }

    @Composable
    fun getOrGenerateTitle(): String{
        return title.ifEmpty {
            stringResource(
                id = R.string.deviceTitle,
                index + 1
            )
        }
    }
    companion object {
        val platformToDrawableMap = mapOf(
            "Sercomm G450" to Pair(R.drawable.vera_plus_big, R.string.vera_plus),
            "Sercomm G550" to Pair(R.drawable.vera_secure_big, R.string.vera_secure),
            "MiCasaVerde VeraLite" to Pair(R.drawable.vera_edge_big, R.string.vera_edge),
            "Sercomm NA900" to Pair(R.drawable.vera_edge_big, R.string.vera_edge),
            "Sercomm NA301" to Pair(R.drawable.vera_edge_big, R.string.vera_edge),
            "Sercomm NA930" to Pair(R.drawable.vera_edge_big, R.string.vera_edge)
        )
    }
}
