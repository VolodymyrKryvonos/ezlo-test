package com.example.ezlo.data.remote.api

import com.example.ezlo.data.remote.model.DeviceList
import retrofit2.http.GET

interface DeviceRest {

    @GET("test_android/items.test")
    suspend fun getListOfDevices(): DeviceList

}