package com.example.ezlo.data.remote.model


import com.example.ezlo.data.local.DeviceEntity
import com.example.ezlo.domain.model.Device
import com.squareup.moshi.Json

data class DeviceDTO(
    @Json(name = "Firmware")
    val firmware: String?,
    @Json(name = "InternalIP")
    val internalIP: String?,
    @Json(name = "LastAliveReported")
    val lastAliveReported: String?,
    @Json(name = "MacAddress")
    val macAddress: String?,
    @Json(name = "PK_Device")
    val pkDevice: Long?,
    @Json(name = "PK_DeviceSubType")
    val pkDeviceSubType: Long?,
    @Json(name = "PK_DeviceType")
    val pkDeviceType: Long?,
    @Json(name = "Platform")
    val platform: String?,
    @Json(name = "Server_Account")
    val serverAccount: String?,
    @Json(name = "Server_Device")
    val serverDevice: String?,
    @Json(name = "Server_Event")
    val serverEvent: String?
) {
    fun toDevice(index: Int) = Device(
        index = index,
        title = "",
        firmware = firmware ?: "",
        pkDevice = pkDevice ?: -1,
        macAddress = macAddress ?: "",
        platform = platform ?: ""
    )

    fun toDeviceEntity() = DeviceEntity(
        title = null,
        firmware = firmware,
        internalIP = internalIP,
        lastAliveReported = lastAliveReported,
        macAddress = macAddress,
        pkDevice = pkDevice,
        pkDeviceSubType = pkDeviceSubType,
        pkDeviceType = pkDeviceType,
        platform = platform,
        serverAccount = serverAccount,
        serverDevice = serverDevice,
        serverEvent = serverEvent
    )
}