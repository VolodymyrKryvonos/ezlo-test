package com.example.ezlo.data.remote.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

data class DeviceList(
    @Json(name = "Devices")
    val devices: List<DeviceDTO>
)