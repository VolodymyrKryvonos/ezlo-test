package com.example.ezlo.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface DeviceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDevice(device: DeviceEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDevices(devices: List<DeviceEntity>)

    @Query("SELECT * FROM devices")
    fun getAllDevices(): Flow<List<DeviceEntity>>

    @Query("DELETE FROM devices")
    suspend fun deleteAllDevices()

    @Delete
    suspend fun deleteDevice(device: DeviceEntity)
    @Query("UPDATE devices SET title = :title WHERE pkDevice = :deviceId")
    suspend fun updateTitleById(deviceId: Long, title: String)
}