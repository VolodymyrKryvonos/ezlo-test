package com.example.ezlo.data.repository

import com.example.ezlo.R
import com.example.ezlo.data.local.DeviceDao
import com.example.ezlo.data.local.DeviceEntity
import com.example.ezlo.data.remote.api.DeviceRest
import com.example.ezlo.domain.model.Device
import com.example.ezlo.domain.repository.DeviceRepository
import com.example.ezlo.ui.util.UiText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class DeviceRepositoryImpl(
    private val deviceApi: DeviceRest,
    private val deviceDao: DeviceDao
) :
    DeviceRepository {
    private val _deviceList = MutableStateFlow<List<Device>>(listOf())
    override val deviceList: StateFlow<List<Device>> = _deviceList.asStateFlow()

    private val _errorMessageFlow = MutableSharedFlow<UiText>()
    override val errorMessageFlow: SharedFlow<UiText> = _errorMessageFlow.asSharedFlow()

    override suspend fun loadDeviceList() {
        withContext(Dispatchers.IO) {
            deviceDao.getAllDevices().collectLatest {
                _deviceList.emit(it.mapIndexed { index, entity -> entity.toDevice(index) })
            }
        }
    }

    override suspend fun resetDeviceList() {
        withContext(Dispatchers.IO) {
            try {
                val response = deviceApi.getListOfDevices().devices
                deviceDao.deleteAllDevices()
                deviceDao.insertDevices(response.map { it.toDeviceEntity() })
            } catch (e: IOException) {
                _errorMessageFlow.emit(UiText.StringResource(R.string.noInternetException))
            } catch (e: HttpException) {
                _errorMessageFlow.emit(
                    UiText.StringResource(
                        R.string.httpException,
                        e.code()
                    )
                )
            } catch (e: Exception) {
                _errorMessageFlow.emit(UiText.StringResource(R.string.unexpectedException))
            }
        }
    }

    override suspend fun updateDeviceTitle(deviceId: Long, newTitle: String) {
        withContext(Dispatchers.IO) {
            deviceDao.updateTitleById(deviceId, newTitle)
        }
    }

    override suspend fun deleteDevice(deviceId: Long) {
        withContext(Dispatchers.IO) {
            deviceDao.deleteDevice(DeviceEntity(pkDevice = deviceId))
        }
    }
}