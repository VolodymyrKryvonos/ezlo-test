package com.example.ezlo.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.ezlo.domain.model.Device

@Entity(tableName = "devices")
data class DeviceEntity(
    val title: String? = null,
    val firmware: String? = null,
    val internalIP: String? = null,
    val lastAliveReported: String? = null,
    val macAddress: String? = null,
    @PrimaryKey
    val pkDevice: Long? = null,
    val pkDeviceSubType: Long? = null,
    val pkDeviceType: Long? = null,
    val platform: String? = null,
    val serverAccount: String? = null,
    val serverDevice: String? = null,
    val serverEvent: String? = null
) {
    fun toDevice(index: Int) = Device(
        index = index,
        title = title?:"",
        firmware = firmware ?: "",
        pkDevice = pkDevice ?: -1,
        macAddress = macAddress ?: "",
        platform = platform ?: ""
    )
}