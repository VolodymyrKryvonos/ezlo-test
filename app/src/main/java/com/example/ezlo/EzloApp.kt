package com.example.ezlo

import android.app.Application
import com.example.ezlo.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EzloApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@EzloApp)
            modules(appModule)
        }
    }

}