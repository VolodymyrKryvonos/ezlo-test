package com.example.ezlo.di

import androidx.room.Room
import com.example.ezlo.Const
import com.example.ezlo.data.local.AppDatabase
import com.example.ezlo.data.remote.api.DeviceRest
import com.example.ezlo.data.repository.DeviceRepositoryImpl
import com.example.ezlo.domain.repository.DeviceRepository
import com.example.ezlo.ui.screens.device.DevicesViewModel
import com.example.ezlo.ui.screens.user.UserViewModel
import com.example.ezlo.util.AppRestBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val appModule = module {
    factory<DeviceRepository> { DeviceRepositoryImpl(provideDeviceRest(),get()) }
    single {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "app_database")
            .build()
    }
    single { get<AppDatabase>().deviceDao() }
    viewModelOf(::DevicesViewModel)
    viewModelOf(::UserViewModel)
}

fun provideDeviceRest(): DeviceRest {
    return AppRestBuilder(DeviceRest::class.java, Const.baseUrl).build()
}
