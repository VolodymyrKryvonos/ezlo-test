package com.example.ezlo.ui.screens.device

import com.example.ezlo.domain.model.Device
import com.example.ezlo.ui.UiEvent

sealed interface DeviceEvents: UiEvent {
    class OpenDeleteDeviceDialogEvent(val device: Device): DeviceEvents
    class OpenDeviceDetailEvent(val device: Device): DeviceEvents
    object CloseDeleteDeviceDialogEvent : DeviceEvents
    object DeleteDeviceEvent: DeviceEvents
    object ResetDeviceList: DeviceEvents
    object EditDeviceEvent: DeviceEvents
    object SaveDeviceChangesEvent: DeviceEvents
    object NavigateBackToDeviceListEvent : DeviceEvents
    class TitleChangeEvent(val title: String): DeviceEvents
}
