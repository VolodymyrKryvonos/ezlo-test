package com.example.ezlo.ui.navigation

import androidx.navigation.NamedNavArgument

sealed class Routes(val destination: String, val arguments: List<NamedNavArgument> = listOf()) {
    object DeviceListScreen : Routes("device-list")
    object DeviceDetailScreen : Routes("device-detail")
}