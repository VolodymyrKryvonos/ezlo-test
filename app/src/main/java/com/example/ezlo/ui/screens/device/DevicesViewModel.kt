package com.example.ezlo.ui.screens.device

import androidx.lifecycle.viewModelScope
import com.example.ezlo.domain.repository.DeviceRepository
import com.example.ezlo.ui.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class DevicesViewModel(private val deviceRepository: DeviceRepository) :
    BaseViewModel<DeviceScreenState, DeviceEvents>() {

    val errorFlow = deviceRepository.errorMessageFlow
    private var newTitle = ""

    init {
        viewModelScope.launch {
            deviceRepository.loadDeviceList()
        }
        viewModelScope.launch {
            deviceRepository.deviceList.collectLatest {
                setState { copy(deviceList = it) }
            }
        }
    }

    override fun setInitialState() = DeviceScreenState()

    override fun handleEvent(event: DeviceEvents) {
        when (event) {
            is DeviceEvents.OpenDeleteDeviceDialogEvent -> {
                setState {
                    copy(
                        selectedDevice = event.device,
                        isDeleteDialogOpen = true
                    )
                }
            }

            DeviceEvents.CloseDeleteDeviceDialogEvent -> {
                setState { copy(selectedDevice = null, isDeleteDialogOpen = false) }
            }

            is DeviceEvents.DeleteDeviceEvent -> deleteDevice()
            DeviceEvents.ResetDeviceList -> resetDeviceList()
            is DeviceEvents.EditDeviceEvent -> editDevice()
            DeviceEvents.SaveDeviceChangesEvent -> saveChanges()
            is DeviceEvents.OpenDeviceDetailEvent -> {
                setState {
                    copy(
                        selectedDevice = event.device,
                        iconState = IconState.Edit
                    )
                }
            }

            is DeviceEvents.TitleChangeEvent -> {
                newTitle = event.title
            }

            DeviceEvents.NavigateBackToDeviceListEvent ->
                setState {
                    copy(
                        iconState = IconState.Reset,
                        isInEditMode = false,
                    )
                }
        }
    }

    private fun resetDeviceList() {
        viewModelScope.launch(Dispatchers.IO) {
            setState { copy(iconState = IconState.Loading) }
            deviceRepository.resetDeviceList()
            setState { copy(iconState = IconState.Reset) }
        }
    }

    private fun deleteDevice() {
        viewModelScope.launch {
            deviceRepository.deleteDevice(state.value.selectedDevice?.pkDevice ?: return@launch)
            setState { copy(selectedDevice = null, isDeleteDialogOpen = false) }
        }
    }

    private fun editDevice() {
        setState { copy(iconState = IconState.Save, isInEditMode = true) }
    }

    private fun saveChanges() {
        viewModelScope.launch {
            deviceRepository.updateDeviceTitle(
                state.value.selectedDevice?.pkDevice ?: return@launch, newTitle
            )
            setState {
                copy(
                    iconState = IconState.Edit,
                    isInEditMode = false,
                    selectedDevice = selectedDevice?.copy(title = newTitle)
                )
            }
            newTitle = ""
        }
    }
}