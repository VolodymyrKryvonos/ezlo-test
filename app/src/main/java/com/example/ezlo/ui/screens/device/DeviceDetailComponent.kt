package com.example.ezlo.ui.screens.device

import android.os.Handler
import android.os.Looper
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.ezlo.R
import com.example.ezlo.domain.model.Device
import com.example.ezlo.ui.screens.user.User
import com.example.ezlo.ui.theme.EzloTheme

@Preview(showBackground = true)
@Composable
fun DeviceDetailComponentPreview() {
    EzloTheme {
        DeviceDetailComponent(
            Device(
                0,
                "Home Number 1",
                3675185172L,
                "23:f2:1a:56:23:e2",
                "1.7.412",
                "Sercomm NA301"
            ),
            user = User()
        )
    }
}

@Composable
fun DeviceDetailComponent(
    device: Device,
    isInEditMode: Boolean = false,
    onEvent: (event: DeviceEvents) -> Unit = {},
    user: User
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 12.dp, vertical = 16.dp)
    ) {
        Header(user) //TODO when nested scroll will fixed move header back to DeviceScreen
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            AsyncImage(
                model = device.getPlatformImageDrawable(),
                contentDescription = stringResource(id = R.string.platform_image),
                modifier = Modifier
                    .size(120.dp)
                    .clip(RoundedCornerShape(10.dp))
            )
            Spacer(modifier = Modifier.width(8.dp))
            TitleEditableField(
                title = device.getOrGenerateTitle(),
                isInEditMode = isInEditMode,
                onTextChange = {
                    onEvent(DeviceEvents.TitleChangeEvent(it))
                }
            )
        }
        Spacer(modifier = Modifier.height(12.dp))
        DeviceInfoTextField(text = stringResource(id = R.string.sn, device.pkDevice))
        DeviceInfoTextField(
            text = stringResource(
                id = R.string.mac_address,
                device.macAddress
            ).uppercase()
        )
        Spacer(modifier = Modifier.height(12.dp))
        DeviceInfoTextField(text = stringResource(id = R.string.firmware, device.firmware))
        DeviceInfoTextField(
            text = stringResource(
                id = R.string.model,
                stringResource(id = device.getModel())
            )
        )
    }
}

@Composable
fun DeviceInfoTextField(text: String) {
    Text(text = text, style = MaterialTheme.typography.bodyLarge)
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TitleEditableField(
    isInEditMode: Boolean = true,
    title: String = "",
    onTextChange: (String) -> Unit,
    textStyle: TextStyle = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.SemiBold)
) {

    var editableTitle by remember {
        mutableStateOf(
            TextFieldValue(title)
        )
    }
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }
    if (isInEditMode) {
        OutlinedTextField(value = editableTitle,
            onValueChange = { newText ->
                val limitedText =
                    if (newText.text.length > 250) {
                        newText.copy(text = newText.text.substring(0..250))
                    } else {
                        newText
                    }
                editableTitle = limitedText
                onTextChange(limitedText.text)
            },
            modifier = Modifier
                .focusRequester(focusRequester)
                .border(0.dp, Color.Transparent, shape = MaterialTheme.shapes.small),

            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Text
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    focusManager.clearFocus(true)
                }
            ),
            colors = OutlinedTextFieldDefaults.colors(
                focusedBorderColor = Color.Transparent,
                unfocusedBorderColor = Color.Transparent,
                disabledBorderColor = Color.Transparent,
                errorBorderColor = Color.Transparent
            ),
            textStyle = textStyle,
            singleLine = true
        )
    } else {
        Text(
            text = editableTitle.text,
            style = textStyle,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
        )
    }
    DisposableEffect(isInEditMode) {
        if (isInEditMode) {
            editableTitle = TextFieldValue(
                text = editableTitle.text,
                selection = TextRange(editableTitle.text.length)
            )
            onTextChange(editableTitle.text)
            Handler(Looper.getMainLooper()).postDelayed(
                { keyboardController!!.show() },
                500
            )
            focusRequester.requestFocus()
        }
        onDispose { }
    }
}
