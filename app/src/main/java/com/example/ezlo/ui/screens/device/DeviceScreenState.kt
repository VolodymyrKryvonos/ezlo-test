package com.example.ezlo.ui.screens.device

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.ezlo.domain.model.Device
import com.example.ezlo.ui.UiState


sealed class IconState(val icon: ImageVector){
    object Loading: IconState(Icons.Default.Refresh)
    object Reset: IconState(Icons.Default.Refresh)
    object Edit: IconState(Icons.Default.Edit)
    object Save: IconState(Icons.Default.Done)
}

data class DeviceScreenState(
    val deviceList: List<Device> = listOf(),
    val selectedDevice: Device? = null,
    val iconState: IconState = IconState.Reset,
    val isDeleteDialogOpen:Boolean = false,
    val isSaveChangesDialogOpen: Boolean = false,
    val isInEditMode: Boolean = false
): UiState