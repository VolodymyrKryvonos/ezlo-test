package com.example.ezlo.ui.screens.device

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.ezlo.R
import com.example.ezlo.domain.model.Device
import com.example.ezlo.ui.navigation.Routes
import com.example.ezlo.ui.screens.user.User
import com.example.ezlo.ui.theme.EzloTheme

@Preview
@Composable
fun DeviceListScreenPreview() {
    EzloTheme {
        Surface {
            DeviceListScreen(state = DeviceScreenState(deviceList = (0..10).map {
                Device(
                    it,
                    "Home Number $it",
                    3675185172L + it,
                    "23:f2:1a:56:23:e2",
                    "1.7.412",
                    "Sercomm NA301"
                )
            }), user = User())
        }
    }
}

@Composable
fun DeviceListScreen(
    state: DeviceScreenState = DeviceScreenState(),
    onEvent: (event: DeviceEvents) -> Unit = {},
    onNewDestination: (destination: String) -> Unit = {},
    user: User
) {
    LazyColumn(
        modifier = Modifier.fillMaxWidth(),
        contentPadding = PaddingValues(vertical = 14.dp)
    ) {
        item {
            Header(user)//TODO when nested scroll will fixed move header back to DeviceScreen
        }
        items(state.deviceList, key = { it.pkDevice }) {
            DeviceItem(it,
                onClick = {
                    onNewDestination(Routes.DeviceDetailScreen.destination)
                    onEvent(DeviceEvents.OpenDeviceDetailEvent(it))
                },
                onLongClick = {
                    onEvent(DeviceEvents.OpenDeleteDeviceDialogEvent(it))
                })
            Box(
                modifier = Modifier
                    .height(3.dp)
                    .fillMaxWidth()
                    .background(MaterialTheme.colorScheme.onBackground)
            )
        }
    }
}

@Preview
@Composable
fun DeviceItemPreview() {
    EzloTheme {
        DeviceItem(
            Device(
                0,
                "Home Number 1",
                3675185172L,
                "23:f2:1a:56:23:e2",
                "1.7.412",
                "Sercomm NA301"
            )
        )
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DeviceItem(
    device: Device,
    onClick: () -> Unit = {},
    onLongClick: () -> Unit = {}
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .combinedClickable(
                onClick = onClick,
                onLongClick = onLongClick
            ),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        AsyncImage(
            model = device.getPlatformImageDrawable(),
            contentDescription = stringResource(id = R.string.platform_image),
            modifier = Modifier
                .size(130.dp)
                .clip(RoundedCornerShape(10.dp))
        )
        Spacer(modifier = Modifier.width(8.dp))
        Column(Modifier.weight(1f)) {
            Text(
                device.getOrGenerateTitle(),
                style = MaterialTheme.typography.bodyLarge,
                fontWeight = FontWeight.SemiBold,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
            )
            Text(
                stringResource(id = R.string.sn, device.pkDevice),
                style = MaterialTheme.typography.bodyMedium
            )
        }
        Icon(
            imageVector = Icons.Default.KeyboardArrowRight,
            contentDescription = null,
            modifier = Modifier.size(34.dp)
        )
        Spacer(modifier = Modifier.width(8.dp))
    }
}
