package com.example.ezlo.ui.screens.user

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

class UserViewModel: ViewModel() {
    private var _user  = mutableStateOf(User())
    val user: State<User> = _user
}

data class User(
    val userName: String = "John Doe",
    val userProfileImage: String? = null
)
