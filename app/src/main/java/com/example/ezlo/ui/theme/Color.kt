package com.example.ezlo.ui.theme

import androidx.compose.ui.graphics.Color

// LIGHT
val PrimaryLight = Color(0xff006493)
val OnLight = Color(0xffffffff)
val ContainerLight = Color(0xffcae6ff)
val OnContainerLight = Color(0xff001e30)

val SecondaryLight = Color(0xff50606e)
val OnSecondaryLight = OnLight
val SecondaryContainerLight = Color(0xffd3e5f6)
val OnSecondaryContainerLight = Color(0xff0c1d29)

val TertiaryLight = Color(0xff65587b)
val OnTertiaryLight = OnLight
val TertiaryContainerLight = Color(0xffebdcff)
val OnTertiaryContainerLight = Color(0xff201634)

val ErrorLight = Color(0xffba1a1a)
val OnErrorLight = Color(0xffffffff)
val ErrorContainerLight = Color(0xffffdad6)
val OnErrorContainerLight = Color(0xff410002)

val BackgroundLight = Color(0xfffcfcff)
val OnBackgroundLight = Color(0xff1a1c1e)
val SurfaceLight = Color(0xfffcfcff)
val OnSurfaceLight = Color(0xff1a1c1e)
val SurfaceVariantLight = Color(0xffdde3ea)
val OnSurfaceVariantLight = Color(0xff41474d)

val OutlineLight = Color(0xff72787e)

// DARK
val PrimaryDark = Color(0xff8dcdff)
val OnDark = Color(0xff00344f)
val ContainerDark = Color(0xff004b70)
val OnContainerDark = Color(0xffcae6ff)

val SecondaryDark = Color(0xffb7c9d9)
val OnSecondaryDark =Color(0xff22323f)
val SecondaryContainerDark = Color(0xff384956)
val OnSecondaryContainerDark = Color(0xffd3e5f6)

val TertiaryDark = Color(0xffcfc0e8)
val OnTertiaryDark = Color(0xff362b4a)
val TertiaryContainerDark = Color(0xff4d4162)
val OnTertiaryContainerDark = Color(0xffebdcff)

val ErrorDark = Color(0xffffb4ab)
val OnErrorDark = Color(0xff690005)
val ErrorContainerDark = Color(0xff93000a)
val OnErrorContainerDark = Color(0xffffdad6)

val BackgroundDark = Color(0xff1a1c1e)
val OnBackgroundDark = Color(0xffe2e2e5)
val SurfaceDark = Color(0xff1a1c1e)
val OnSurfaceDark = Color(0xffe2e2e5)
val SurfaceVariantDark = Color(0xff41474d)
val OnSurfaceVariantDark = Color(0xffc1c7ce)

val OutlineDark = Color(0xff8b9198)