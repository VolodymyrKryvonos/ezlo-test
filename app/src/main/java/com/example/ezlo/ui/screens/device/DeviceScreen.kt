package com.example.ezlo.ui.screens.device

import android.content.res.Configuration
import android.os.Bundle
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.Wallpapers
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import com.example.ezlo.R
import com.example.ezlo.domain.model.Device
import com.example.ezlo.ui.navigation.Routes
import com.example.ezlo.ui.screens.user.User

@Preview(
    showBackground = true,
    device = "spec:width=1920px,height=1080px,dpi=640", fontScale = 1.3f,
    showSystemUi = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    wallpaper = Wallpapers.YELLOW_DOMINATED_EXAMPLE
)
@Composable
fun DeviceScreenPreview() {
    DeviceScreen(
        padding = PaddingValues(8.dp),
        state = DeviceScreenState(deviceList = (1..10).map {
            Device(
                it,
                "Home Number $it",
                3675185172L + it,
                "23:f2:1a:56:23:e2",
                "1.7.412",
                "Sercomm NA301"
            )
        }),
        user = User(),
    ) {}
}

@Composable
fun DeviceScreen(
    padding: PaddingValues,
    state: DeviceScreenState,
    user: User,
    onEvent: (DeviceEvents) -> Unit,
) {
    val navController = rememberNavController()
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(padding)
    ) {
        NavContainer(navController, state, user, onEvent)
    }
    DeleteDialog(
        state.isDeleteDialogOpen,
        state.selectedDevice?.getOrGenerateTitle() ?: "",
        onCancelClicked = { onEvent(DeviceEvents.CloseDeleteDeviceDialogEvent) },
        onConfirmClicked = { onEvent(DeviceEvents.DeleteDeviceEvent) }
    )
    DestinationChangeListener(navController, onEvent)
}

@Composable
fun DestinationChangeListener(navController: NavController, onEvent: (DeviceEvents) -> Unit) {
    DisposableEffect(Unit) {
        val listener =
            NavController.OnDestinationChangedListener { _: NavController, destination: NavDestination, _: Bundle? ->
                val currentDestination = destination.route
                if (currentDestination == Routes.DeviceListScreen.destination) {
                    onEvent(DeviceEvents.NavigateBackToDeviceListEvent)
                }
            }
        navController.addOnDestinationChangedListener(listener)
        onDispose {
            navController.removeOnDestinationChangedListener(listener)
        }
    }
}

@Composable
fun DeleteDialog(
    isDeleteDialogOpen: Boolean,
    deviceTitle: String,
    onConfirmClicked: () -> Unit,
    onCancelClicked: () -> Unit
) {
    if (isDeleteDialogOpen) {
        AlertDialog(
            onDismissRequest = { onCancelClicked() },
            title = { Text(text = stringResource(id = R.string.deleteDevice)) },
            text = { Text(text = stringResource(id = R.string.deleteDeviceMsg, deviceTitle)) },
            confirmButton = {
                TextButton(
                    onClick = { onConfirmClicked() }
                ) {
                    Text(text = stringResource(id = R.string.ok))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = { onCancelClicked() }
                ) {
                    Text(text = stringResource(id = R.string.cancel))
                }
            }
        )
    }
}

@Composable
fun Header(user: User) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        ProfileImagePlaceholder(imageResource = user.userProfileImage)
        Text(text = user.userName, style = MaterialTheme.typography.headlineLarge)
    }
}

@Composable
fun ProfileImagePlaceholder(
    size: Dp = 150.dp,
    imageResource: String? = null
) {

    AsyncImage(
        model = imageResource ?: R.drawable.ic_acount_icon_placeholder,
        contentDescription = "Profile Image Placeholder",
        modifier = Modifier
            .size(size)
            .clip(CircleShape),
        colorFilter = if (imageResource == null) {
            ColorFilter.tint(MaterialTheme.colorScheme.onBackground)
        } else {
            null
        }
    )
}

@Composable
fun NavContainer(
    navController: NavHostController,
    deviceScreenState: DeviceScreenState,
    user: User,
    onEvent: (DeviceEvents) -> Unit
) {
    NavHost(
        navController = navController,
        startDestination = Routes.DeviceListScreen.destination
    ) {
        composable(route = Routes.DeviceListScreen.destination) {
            DeviceListScreen(
                state = deviceScreenState,
                user = user,
                onEvent = onEvent,
                onNewDestination = navController::navigate
            )
        }
        composable(
            route = Routes.DeviceDetailScreen.destination
        ) {
            if (deviceScreenState.selectedDevice == null) {
                navController.navigateUp()
                return@composable
            }
            DeviceDetailComponent(
                device = deviceScreenState.selectedDevice,
                user = user,
                isInEditMode = deviceScreenState.isInEditMode,
                onEvent = onEvent
            )
        }
    }
}