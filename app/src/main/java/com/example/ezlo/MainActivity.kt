package com.example.ezlo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.ezlo.ui.screens.device.DeviceEvents
import com.example.ezlo.ui.screens.device.DeviceScreen
import com.example.ezlo.ui.screens.device.DevicesViewModel
import com.example.ezlo.ui.screens.device.IconState
import com.example.ezlo.ui.screens.user.UserViewModel
import com.example.ezlo.ui.theme.EzloTheme
import com.example.ezlo.ui.util.UiText
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {
    private val devicesViewModel: DevicesViewModel by viewModel()
    private val userViewModel: UserViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val snackbarHostState = remember { SnackbarHostState() }
            val deviceScreenState = devicesViewModel.state.collectAsStateWithLifecycle()
            val errorMessagesState = devicesViewModel.errorFlow.collectAsStateWithLifecycle(
                initialValue = UiText.DynamicString("")
            )

            LaunchedEffect(errorMessagesState.value) {
                val message = errorMessagesState.value.asString(this@MainActivity)
                if (message.isNotEmpty()) {
                    snackbarHostState.showSnackbar(message)
                }
            }
            EzloTheme {
                Scaffold(
                    snackbarHost = {
                        SnackbarHost(
                            snackbarHostState,
                            snackbar = { ErrorSnackbar(it.visuals.message) })
                    },
                    topBar = { TopBar(deviceScreenState.value.iconState) }
                ) { padding ->
                    DeviceScreen(padding, deviceScreenState.value,userViewModel.user.value, devicesViewModel::handleEvent)
                }
            }
        }
    }

    @Composable
    fun ErrorSnackbar(message: String) {
        Snackbar(
            containerColor = MaterialTheme.colorScheme.errorContainer,
            contentColor = MaterialTheme.colorScheme.error,
            modifier = Modifier.padding(10.dp)
        ) {
            Text(text = message)
        }
    }

    @Composable
    fun TopBar(iconState: IconState) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(64.dp)
                .background(MaterialTheme.colorScheme.primary)
                .padding(10.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(id = R.string.app_name),
                style = MaterialTheme.typography.titleLarge
            )

            IconButton(onClick = {
                when (iconState) {
                    IconState.Edit -> devicesViewModel.handleEvent(DeviceEvents.EditDeviceEvent)
                    IconState.Loading -> Unit
                    IconState.Reset -> devicesViewModel.handleEvent(DeviceEvents.ResetDeviceList)
                    IconState.Save -> devicesViewModel.handleEvent(DeviceEvents.SaveDeviceChangesEvent)
                }
            }, enabled = iconState != IconState.Loading) {

                if (iconState != IconState.Loading) {
                    Icon(imageVector = iconState.icon, contentDescription = null)
                } else {
                    CircularProgressIndicator(color = MaterialTheme.colorScheme.onPrimary)
                }
            }
        }
    }
}

