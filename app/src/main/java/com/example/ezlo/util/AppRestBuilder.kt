package com.example.ezlo.util

class AppRestBuilder<T>(
    private val service: Class<T>,
    private val baseUrl: String
) {

    private var apiFactory: ApiFactory = ApiFactory()

    fun build(): T {
        return apiFactory
            .buildRetrofit(baseUrl)
            .create(service)
    }

}