# eZLO test

This project is a mobile application that displays devices fetched from an API, with the possibility to edit and delete them locally. The app is built using the latest Android technologies, including Jetpack Compose for the user interface, Clean Architecture for the application architecture, and dynamic colors from Material 3.

## Project Overview

### The main features of the app are as follows:

1. Fetch devices from the API and display them in a list.
2. Allow users to edit device information and save changes locally.
3. Enable users to delete devices, removing them from the local storage.

### Architecture

The project follows the Clean Architecture principles, which involve separating the application into layers for better maintainability and testability. The main layers are:

1. Presentation Layer (Compose UI): Handles UI logic using Jetpack Compose.
2. Domain Layer: Contains the use cases and business logic.
3. Data Layer: Handles data fetching and storage, including API calls and local database management.

### How to Build and Run

To build and run the app, follow these steps:

1. Clone the project repository to your local machine.
2. Open the project in Android Studio.
3. Connect a physical Android device or start an Android emulator.
4. Click on the "Run" button in Android Studio to install and launch the app on the device/emulator.